--*** Shagball Online!  *--by hilaire9      May 2001
Type: Lego Extreme   Length: A small Shagball Arena
Zip name: 'shag'     Zip size 993Kb
--Description:  IT'S NOT A RACE! It's Shagball Online! Shagball is a
                soccer type game designed for Online play between two
                or more players. 
            
-***To Install: Unzip 'shag' into the main Re-Volt folder. 

                       **  How to Shag **
1.) Before the game starts the Host divides all Players into 
    two teams: the Blue Team and the Red Team.  

2.) The Host decides which cars will be used in the game.  
    NOTE: Amateur cars with flat fronts and backs handle
          the ball better than Pro cars such as Toyeca, etc.
      
3.) There are 10 Soccer balls and 1 Beachball on the playing
    area. 

4.) When the Game begins Members of the Blue Team push as many 
    balls as they can into the Blue Goal. The Red Team does the 
    same into the Red Goal. 

5.) The Game is over when no more balls are to be found on the 
    playing area.

6.) The Team with the most balls in their net wins!  
                        
                             ** Rules **
            1.) You will be repositioned if you try to enter the
                opponent's Goal in an attempt to knock out a ball.           
            2.) You are allowed to knock balls out of an opponent's
                Goal with a Weapon (Water Balloons, etc.). But you
                must stop when the game is over.
            3.) If a Player crashes out of a game; the Team must
                continue without that Player. If you find yourself 
                all alone in the game, you win. 
            4.) In Single-Player mode (Offline): the Player is the
                Blue team, the CPU Cars are the Red Team. After
                the game if 1 or more balls are found in the Red 
                Goal: you lose! Win or lose, it's good for Target
                Practice on slowly moving CPU cars. Pickups on.  
                The offline game is worthless.              
                           
**Bugs and Issues: Occasional unwanted repositions. This is only an
                   experiment in a different mode of play for Re-Volt. 
                         
--***Tools/Credit: Re-Volt Track Editor/MAKEITGOOD, Adobe, Paint,
                    Ali's rvGlue, Ekla prm, and textures off the Web.
                    Various images from the Austin Powers movies.

                                  