********************************************************************************
**                               Changelog                                    **
********************************************************************************
--------------------------------------------------------------------------------
Version 20.1116a

- #1 Update

Fixes (Santiii727):
- Moved the start of each game, now it is in front of the ball.
- Added redbook

--------------------------------------------------------------------------------

Version 18.0707a

- First Public release (Campo ufficiale da calcio omologato ASBA>> ''Official ASBA approved football field'' - Balilla Soccer Cup 2018)

--------------------------------------------------------------------------------
Name: ASBA Soccer Arena
Release date: June 2018
Track Type: Voltball (Multiplayer only)
Difficulty: Easy
--------------------------------------------------------------------------------
Note: It was originally created in 2018 for the tournament Balilla Soccer Cup 2018.

All the Base arena is created by Lo Scassatore

You need RVGL to play this: https://rvgl.re-volt.io/

- Lo Scassatore, Santiii727

re-volt.io

©️ 2018-2020 Alessandro Ferrentino (Lo Scassatore) and RVGL I/O Community.
