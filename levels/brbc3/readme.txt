This battle course is originally made by Nintendo Co. Ltd. for the game "Mario Kart Super Circuit" and then remade as a retro track for the game "Mario Kart Wii".
This is a coversion using the models from Mario Kart Wii and adapted to fit the size of RVGL cars. I do not own this track's model, textures or concept. 

Creator notes: This is the 6th of (I hope) multiple conversions from Mario Kart Wii. The music is integrated in the game as a 3d sound in the hope we get support for intro+loop music in RVGL.

Technical notes: The sand in the middle of the course is drivable, although will slow you down quite a bit (I tried to recreate the offroad effect from Mario Kart Wii). The same goes for the grass even though it's not drivable in Mario Kart Wii).
You can drive wherever you want, even on the sides of the track if you really feel the need to, except on the grass outside the track. The whole track is enclosed in invisible walls so it should be impossible to get out of the course.
You can jump over the inner walls and land on the grass that's enclosed within.
I copied the item sets from MKWii.
In this track there will always be 17 pickups (+ 4 star positions) regardless of the number of players. This allows for even matches where everyone can get an item. The side effect is that races will a bit more chaotic, but I prefer that to unfair. 
I rated this track as "Easy", since the course has a very simple, easy to understand design.

Thank you for playing RVGL in 2020 and for downloading this track. I hope you enjoy.

You may not publish an edited version this course without my permission, unless I've been out of the RVGL scene for a couple years. If I leave and return, I reserve any right to undo any edit you made to the course.
This course is part of the MKWii Conversion Project for RVGL. It will include (I hope) all the 32 stock tracks in Mario Kart Wii (and all the 10  battle course), complete with reverse version if possible (sorry, no reverse on Rainbow Road).