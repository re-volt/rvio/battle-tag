Thank you for taking the time to open this (whether intentionaly or not). Please keep reading.
____________________________________________________
**IMPORTANT**
THIS LEVEL REQUIRES RVGL 20.0430a (or higher) TO RUN. 

____________________________________________________

HOME BATTLE

Length: n/a
Length(R):  n/a
Difficulty: n/a


Created: 06/21/2021


____________________________________________________

Description:

Bring the fight next door! 
A small arena featuring a grand living room with atrium and a cozy kitchen with a breakfast nook. 

____________________________________________________

Patch - 9/14/2022

-Fixed flickering shadow on reflective floors
-Fixed instance errors
-Added doormat.prm
-Added decor2.prm
____________________________________________________

Developers:

GJ
-Models
-Textures
-Objects
-Animations
-Lighting and Shading
-Visiboxes

I Spy
-Original Theme Pitch and Idea
-Color Design and Pallette
-Visual Design Advisor
-Animation Advisor
-Concept Art

Made with Blender Plugin
____________________________________________________

Stats:

World Faces: 4,109
NCP Faces: 674
Development Time: 1.5 Months 


____________________________________________________



Trivia:

-Jumping down from the second floor balcony is repurposed from an early version of Home 2. 
-The pet food bowl has the name of a childhood pet.
-The pie was delicious
-The arena was designed to feel peculiar, like that one friends house you went to when you were a kid and thought "this is so different and weird... you live here?" and then you never visit again. 
-picture1.prm includes a 1950 painting by Rice Pereira.
-picture2.prm includes a 1943-44 painting by Arshile Gorky
-picture3.prm is a portriat of Re-Volt dev Anthony Rosbottom at his office.


____________________________________________________
IMPORTANT
-Items in this directory are built and designed by GJ and I SPY.
-Please consult GJ before altering or reusing assets outside of personal use. (I'm lenient about asset use, don't be afraid to ask)
-Feel free to distribute as is without taking credit from the developers.
-Do not alter this Readme file, and include it in this directory if distributing to friend, family, or whomever. 
-This track directory is free to download and use, it is not for sale and not to be sold. Period.

Re-Volt Hideout
https://forum.re-volt.io/memberlist.php?mode=viewprofile&u=2090

Email (I don't check this too often)
grnthn2@gmail.com

Discord
.?#8017  (replace ? with coffee mug emoji)

I SPY is reclusive, but if all else fails you can try to contact him instead. He also isn't a spy that's just his name.




We hope you thoroughly enjoy this track,
Thank you  :)