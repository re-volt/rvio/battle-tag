____________________________________________________
**IMPORTANT**
THIS LEVEL REQUIRES RVGL 20.0905a (or higher).

____________________________________________________

DOWNTOWN Battle

Length: n/a
Length(R):  n/a
Difficulty: n/a

Created: 07/01/2023


____________________________________________________

Description:

Get ready for some road rage!
Fight in the large open square and surrounding paths. Dodge moving cars as they circle the clock!


____________________________________________________

Patch - 7/9/2023

-Fixed minor vertex color errors and a flipped face.

____________________________________________________

Patch - 10/14/2023

-Behind the start position: Replaced construction fence variant with the correct backlit one.
-Edgeflow and gaps in mesh fixed at both ends of the police station.
-Fixed visibox error in the tunnel next to fire station.
-Repaired vertex shading on the blocked Town Hall path 
-As well as vertex color (and double sided flags as needed) on brick wall near graffiti

____________________________________________________

Patch - 8/25/2024

-Adjusted car wheel collision
-Fixed gaps in the bank windows
-Bike instance has now accurately has 1 gear crankset instead of 2
-Fixed instance priorities for low performance mode

____________________________________________________

Developers:

GJ
-Models
-Textures
-Objects
-Animations
-Lighting and Shading
-Visiboxes

I Spy
-Color Design and Pallette
-Visual Design Advisor
-Animation Advisor
-Sound Recording and Curation

Music is Toys in The Hood Demo

Made with Blender Plugin

____________________________________________________

Stats:

World Faces: 6,204
With Instances: 9,667
NCP Faces: 922
Development Time: 25 Days


____________________________________________________


Trivia:

-The amount of custom object scripting for all the city tracks is longer than a space opera I once wrote.
-The donut shop sells the best donuts in New Volt City.
-The grafitti has four car names competing for the brick space as a fun detail, and a loose reference to four-player battle tag in split screen mode.


____________________________________________________
IMPORTANT
-Items in this directory are built and designed by GJ and I SPY.
-Please consult GJ before altering or reusing assets outside of personal use. (I'm lenient about asset use, don't be afraid to ask)
-Feel free to distribute as is without taking credit from the developers.
-Do not alter this Readme file, and include it in this directory if distributing to friend, family, or whomever. 
-This track directory is free to download and use, it is not for sale and not to be sold. Period.

Re-Volt Hideout
https://forum.re-volt.io/memberlist.php?mode=viewprofile&u=2090

Email (I don't check this too often)
grnthn2@gmail.com

Discord
.?#8017  (replace ? with coffee mug emoji)

I SPY is reclusive, but if all else fails you can try to contact him instead. He also isn't a spy that's just his name.




We hope you thoroughly enjoy this track,
Thank you  :)