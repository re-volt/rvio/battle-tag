Thank you for taking the time to open this (whether intentionaly or not). Please keep reading.

____________________________________________________

ROOFTOPS BATTLE

Game Type: Battle Tag
Length: n/a
Difficulty: n/a


Created: 12/13/2020

____________________________________________________

Description:

A medium sized battle tag arena lit by city lights, just before dawn.  
Compete against other players on multitiered buildings surrounding a rooftop radio antenna. 


____________________________________________________

Developers:

Acclaim/Probe: 
-Geometry
-Textures
-Music composing (Overdriver)

GJ
-Additional Meshes
-Layout
-Texture Mapping and new Textures
-Additional Instances
-Gouraud Shading

I Spy
-Visual Design Advisor
-Layout

____________________________________________________

Stats:

Faces: ~7,874 
NCP Faces: ~1,852
Initial Development Time: 2 weeks, and an adiitional day of fixing the edgeflow. 

This is our first finished battle track, made in December 2019 but unreleased until now.

____________________________________________________

Trivia:
Battle tracks tend to scoop up cut content. The hotel sign jump is from a way earlier version of Rooftops 1.
The skybox is the unused one from Acclaim's Rooftops on the dreamcast, painstakingly rebuilt.
The tower in the middle of the track is based on concept art for Fiddlers on the Roof.



____________________________________________________
Feel free to distribute the track and use any assets in the directory.
On the condition that you're sure to give credit where it is due. 