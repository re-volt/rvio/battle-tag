===TRACK SUMMARY===

Name:          TM4: Bedroom
Mode:          Battle Tag
Converted by:  mrroblinx
Creation date: October 6th to 13th 2021

===DESCRIPTION===

My first battle tag addition. I feel as though the Twisted Metal arenas are perfect for this mode, and I had a ton of fun working on this one.


Keep in mind that you can destroy the gas cans and other stuff in the level when you run into them, opening up new areas to explore.

===INCLUDED IN THIS TRACK==

Custom animations (Objects exploding, Starting prepare screen, and a few others).
Custom powerups based on Twisted Metal 4.

===TOOLS USED===

Blender (with Marv's plugin)
Paint.NET
prm2hul
TM4 Map ripper
PVV (for ripping textures that didn't come with the models)

===SPECIAL THANKS===

charlieamer, for the TM4 Map ripper and the car models.
Matsilagi, for beta testing.
The Re-Volt community.