TENT OF TERROR BATTLE

This is my third Re-Volt arena - it's my track entry for the 2023 Re-Volt World Autumn Party.

Name - Tent of Terror Battle
Author - The Green J
Software used - Blender, Paint.net, PowerPoint, Audacity, prm2hul, RVGL's MAKEITGOOD, LMMS

Story:
The Rumbling Bros. Circus was founded in 1921 and rose to prominence throughout the years as the greatest American single-ring circus attraction until the attack on Pearl Harbor in late 1941. That incident not only brought the circus to an end, but the growth of television made the closure permanent, driving the last nail in its coffin.

As a result, the circus grounds, last situated in an unidentified area of the northeastern United States, were left untouched, and nothing has yet been removed. To make matters worse, supernatural forces have been invading the circus for the past few decades, and now, it is the ideal location for Toy-Volt Battle Tag competitions.

Trivia:
- There are references to past tracks and arenas:
 - The unicycle and crates are from Berryland Muffinworks. The former has a different-colored texture.
 - The metal grid is from Theater Battle.
 - The ball is from Warped Farmhouse Battle.
 - The music in the arena plays the first part of the Lava Circus theme.
- This map is as original as it can get and has absolutely nothing to do with a particular popular recent web-animated series.

Inspirations:
- CarnEvil (1998 Arcade Game), for the haunted circus idea

Credits and thank-you's:
- Xarc, for the skybox from Castle 1 and 2.
- Acclaim, for the game itself and some textures.
- Used sounds:
 - https://freesound.org/people/tim.kahn/sounds/151827/ - Creative Commons, Attribution 4.0 License
 - https://freesound.org/people/beautifuldaymonster1968/sounds/640984/
- Reused game sounds from:
 - Dr. Seuss' The Cat in the Hat (c) 2003 Vivendi Universal, Universal, DreamWorks, Magenta Software, and Theodor "Dr. Seuss" Geisel himself
- Frank Wen, for the Fluid R3 SoundFont I used for my original composition for this track.
- Last, but not least: The Re-Volt community as a whole, for keeping the game alive for years (and decades)!